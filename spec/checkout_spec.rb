# frozen_string_literal: true

describe Checkout do
  let(:promotional_rules) do
    [
      Rule.new(
        condition: { greater_price: 60 },
        modification: { percentage_discount: 10 }
      ),
      Rule.new(
        filter: { product_code: '001' },
        condition: { min_count: 2 },
        modification: { item_price: 8.5 }
      )
    ]
  end

  let(:red_scarf) do
    Item.new(product_code: '001', name: 'Ref Scarf', price: 9.25)
  end

  let(:silver_cufflinks) do
    Item.new(product_code: '002', name: 'Silver cufflinks', price: 45.0)
  end

  let(:silk_dress) do
    Item.new(product_code: '003', name: 'Silk Dress', price: 19.95)
  end

  it 'creates object with promotional rules' do
    expect do
      Checkout.new(promotional_rules)
    end.not_to raise_error
  end

  it 'contains promotional rules inside object' do
    checkout = Checkout.new(promotional_rules)
    expect(checkout.private_methods).to include(:promotional_rules)
    expect(checkout.send(:promotional_rules)).to eql(promotional_rules)
  end

  describe '#total' do
    it 'returns 0 by default' do
      checkout = Checkout.new(promotional_rules)
      expect(checkout.total).to eql(0)
    end
  end

  describe 'discounts' do
    context '10% off when over £60' do
      it 'contains correct price' do
        co = Checkout.new(promotional_rules)
        co.scan(silver_cufflinks)
        co.scan(silver_cufflinks)

        expect(co.total).to eql(81.0)
      end
    end

    context "set new price for 'Ref Scarf' when contains 2 items" do
      it 'contains correct price' do
        co = Checkout.new(promotional_rules)
        co.scan(red_scarf)
        co.scan(red_scarf)
        expect(co.total).to eql(17.0)
      end
    end
  end

  it 'first example' do
    co = Checkout.new(promotional_rules)
    co.scan(red_scarf)
    co.scan(silver_cufflinks)
    co.scan(silk_dress)
    expect(co.total).to eql(66.78)
  end

  it 'second example' do
    co = Checkout.new(promotional_rules)
    co.scan(red_scarf)
    co.scan(silk_dress)
    co.scan(red_scarf)
    expect(co.total).to eql(36.95)
  end

  it 'second example' do
    co = Checkout.new(promotional_rules)
    co.scan(red_scarf)
    co.scan(silver_cufflinks)
    co.scan(red_scarf)
    co.scan(silk_dress)
    expect(co.total).to eql(73.76)
  end
end
