# frozen_string_literal: true

class Item
  attr_reader :product_code, :name, :original_price
  attr_accessor :price

  def initialize(product_code:, name:, price:)
    @product_code = product_code
    @name = name
    @original_price = price
    @price = price
  end
end
