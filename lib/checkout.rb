# frozen_string_literal: true

require_relative 'item'
require_relative 'rule'

class Checkout
  attr_reader :total, :basket

  def initialize(promotional_rules)
    @promotional_rules = promotional_rules
    @total = 0
    @basket = []
    @promo_items = []
  end

  def scan(item)
    @basket << item

    apply_promotions

    @total = (basket + promo_items).inject(0) { |sum, it| sum + it.price }
  end

  private

  attr_reader :promotional_rules, :promo_items

  def apply_promotions
    @promo_items = []
    promotional_rules.each do |rule|
      collection = rule.filter ? find_subset(rule.filter) : basket
      next unless promotional_condition_pass(collection, rule.condition)

      modify_prices(collection, rule.modification)
    end
  end

  def find_subset(rule)
    basket.select { |item| item.send(rule.keys[0]) == rule.values[0] }
  end

  def promotional_condition_pass(collection, rule)
    case rule.keys[0]
    when :greater_price
      true if collection.map(&:price).sum > rule.values[0]
    when :min_count
      true if collection.size >= rule.values[0]
    end
  end

  def modify_prices(collection, rule)
    return unless %i(percentage_discount item_price).include?(rule.keys[0])

    send("modify_prices_with_#{rule.keys[0]}", collection, rule.values[0])
  end

  def modify_prices_with_percentage_discount(_collection, value)
    name = "#{value}% discount"
    sum = basket.inject(0) { |acc, it| acc + it.price }
    price = (sum * value / 100.0 * -1).ceil(2)
    @promo_items << Item.new(product_code: 'PROMO', name: name, price: price)
  end

  def modify_prices_with_item_price(collection, value)
    @basket = @basket.each do |item|
      next unless item.product_code == collection[0].product_code

      item.price = value.round(2)
    end
  end
end
