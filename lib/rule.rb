# frozen_string_literal: true

class Rule
  attr_reader :filter, :condition, :modification

  def initialize(filter: nil, condition:, modification:)
    @filter = filter
    @condition = condition
    @modification = modification
  end
end
